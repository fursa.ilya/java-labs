package com.fursa.task10;

/**
 * 1. Сложение матриц
 */
public class Solution {

    public static void main(String[] args) {
        int a[][]={{1,2,3}, {4,5,6}, {7,8,9}}; //Объявляем первую матрицу
        int b[][]={{1,2,3}, {4,5,6}, {7,8,9}}; //Объявляем вторую матрицу
        int c[][] = new int[3][3]; //Матрица хранящая результат сложения

        for (int i = 0; i < 3; i++) { //проходимся по строкам и столбцам матрицы
            for (int j = 0; j < 3; j++) {
                c[i][j] = a[i][j] + b[i][j]; //каждый c[i][j] элемент развен сумме a[i][j] и b[i][j]
                 System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }

}
