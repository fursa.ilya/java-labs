package com.fursa.task8;

import java.util.Arrays;

/**
 * Сортировка пузырьком
 */
public class Solution {

    public static void main(String[] args) {
        int[] arr = { 64, 42, 12, 90, 91, 67, 45, -1, 321, 65, 10, 17, 15, -1, 55, 34, 40, 900, 231 };
        System.out.println("Исходный массив: " + Arrays.toString(arr));
        //проходимся дважды по массиву, если arr[i] > arr[j] меняем местами
        //Самый базовый алгоритм сортировки
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if(arr[i] > arr[j]) {
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        System.out.println("Результирующий массив: " + Arrays.toString(arr));


    }
}
