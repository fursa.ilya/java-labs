package com.fursa.task17;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person{
    private List<Lesson> lessons = new ArrayList<>(); //У студента имеется список дисциплин

    //Базовый конструктор
    public Student(String name, String surname, int age) {
        super(name, surname, age);
    }

    //Студент может добавлять дисциплину
    public void addLesson(Lesson lesson) {
        if(lessons.size() >= 30) {
            System.out.println("Студент не может выбирать больше 30 курсов");
        } else {
            lessons.add(lesson);
        }
    }

    //Студент может добавлять оценку по предмету
    public void addMark(Lesson lesson, int mark) {
        for (int i = 0; i < lessons.size(); i++) {
           Lesson current = lessons.get(i);
           if(current.getTitle().equals(lesson.getTitle())) {
               current.setMark(mark);
           }
        }
    }

    //печать средней оценки
    public void printAverageMark() {
        int sum = 0;
        for (int i = 0; i < lessons.size(); i++) {
            Lesson current = lessons.get(i);
            sum += current.getMark();
        }

        System.out.println("Средняя оценка студента: " + sum / lessons.size());
    }


}
