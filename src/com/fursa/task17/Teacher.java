package com.fursa.task17;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {
    private List<Student> students = new ArrayList<>();
    private List<Lesson> lessons = new ArrayList<>();

    public Teacher(String name, String surname, int age) {
        super(name, surname, age);
    }

    public void printStudents() {
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            System.out.println(student);
        }
    }

    public void printLessons() {
        for (int i = 0; i < lessons.size(); i++) {
            Lesson lesson = lessons.get(i);
            System.out.println(lesson);
        }
    }

    public void printLessonsCount() {
        System.out.println(lessons.size()); //Сколько всего преподаватель читает дисциплин
    }

    //Добавляем преподавателю студента
    public void addStudent(Student student) {
        students.add(student);
    }

    //Добавляем дисциплину которую читает преподаватель
    public void addLesson(Lesson lesson) {
        if(lessons.size() >= 5) {
            System.out.println("У учителя не может быть больше 5 уроков");
        } else {
            lessons.add(lesson);
        }
    }
}
