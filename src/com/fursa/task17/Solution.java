package com.fursa.task17;

import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализовать суперкласс Person (человек) и его подклассы Student (Студент) и Teacher (преподаватель)
 * в соответствии с диаграммой классов на Рис. 4.1. Студент может выбирать изучаемые курсы
 * (в количестве не более 30)
 * и получать за них соответствующие оценки, а также добавлять курс и оценку за него, печатать перечень изучаемых
 * курсов и среднюю за них оценку. Для Преподавателя количество курсов – это количество
 * дисциплин, читаемых в настоящее время (не более 5).
 */
public class Solution {
    private static List<Lesson> lessons;
    private static List<Student> students;
    private static List<Teacher> teachers;

    //Заполняем списки в static блоке
    //чит https://ru.wikipedia.org/wiki/%D0%91%D0%BB%D0%BE%D0%BA_%D0%B8%D0%BD%D0%B8%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8
    static {
        lessons = new ArrayList<>();
        lessons.add(new Lesson("Химия", 40));
        lessons.add(new Lesson("Физика", 45));
        lessons.add(new Lesson("Биология", 48));
        lessons.add(new Lesson("Математика", 60));

        students = new ArrayList<>();
        students.add(new Student("Илья", "Макаров", 19));
        students.add(new Student("Алексей", "Петров", 20));
        students.add(new Student("Алина", "Ипатова", 18));
        students.add(new Student("Мария", "Сидорова", 21));

        teachers = new ArrayList<>();
        teachers.add(new Teacher("Афанасий", "Петров", 40));
        teachers.add(new Teacher("Маргарита", "Масленникова", 29));
        teachers.add(new Teacher("Федор", "Игнатьев", 55));
        teachers.add(new Teacher("Егор", "Летов", 45));
    }

    public static void main(String[] args) {
        Student student = students.get(1);
        Student student2 = students.get(2);
        Student student3 = students.get(3);

        student.addLesson(lessons.get(1));
        student.addLesson(lessons.get(2));
        student.addLesson(lessons.get(3));
        student.addLesson(lessons.get(3));
        student.addMark(lessons.get(1), 3);
        student.addMark(lessons.get(2), 2);
        student.addMark(lessons.get(3), 4);

        student.printAverageMark();
        System.out.println("Name " + student.getName());
        System.out.println("Surname " + student.getSurname());
        System.out.println("Age " + student.getAge());

        Teacher teacher = teachers.get(2);
        teacher.addStudent(student);
        teacher.addStudent(student2);
        teacher.addStudent(student3);
        teacher.addLesson(lessons.get(1));
        teacher.addLesson(lessons.get(2));

        System.out.println("---------------Teacher---------------");

        System.out.println("Name " + teacher.getName());
        System.out.println("Surname " + teacher.getSurname());
        System.out.println("Age " + teacher.getAge());
        teacher.printLessonsCount(); //Сколько дисциплин ведет учитель
        teacher.printStudents(); //Какие студенты учатся у него

    }
}
