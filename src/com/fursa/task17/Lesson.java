package com.fursa.task17;

//Класс представляющий дисциплину
public class Lesson {
    private String title; //Название дисциплины
    private int hours; //Количество часов курса
    private int mark; //Оценка за курс

    public Lesson(String title, int hours, int mark) {
        this.title = title;
        this.hours = hours;
        this.mark = mark;
    }

    public Lesson(String title, int hours) {
        this.title = title;
        this.hours = hours;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "title='" + title + '\'' +
                ", hours=" + hours +
                ", mark=" + mark +
                '}';
    }
}
