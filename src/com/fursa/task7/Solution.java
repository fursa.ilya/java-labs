package com.fursa.task7;

import java.util.Arrays;

/**
 * Сортировка методом выбора максимума
 */
public class Solution {

    public static void main(String[] args) {
        int[] arr = { 64, 42, 12, 90, 91, 67, 45, -1, 321, 65, 10, 17, 15, -1, 55, 34, 40, 900, 231 };
        System.out.println("Исходный массив: " + Arrays.toString(arr));

        for (int i = 0; i < arr.length; i++) {
            int maxIndex = max(arr, i);
            int tmp = arr[i];
            arr[i] = arr[maxIndex];
            arr[maxIndex] = tmp;
        }

        System.out.println("Результат сортировки: " + Arrays.toString(arr));
    }

    private static int max(int[] arr, int start) {
        int maxIndex = start;
        int maxValue = arr[start];
        for (int i = start + 1; i < arr.length; i++) {
            if(arr[i] > maxValue) {
                maxValue = arr[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }
}
