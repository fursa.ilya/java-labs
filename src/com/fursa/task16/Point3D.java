package com.fursa.task16;

public class Point3D extends Point {
    private double z;

    //Конструктор по умолчанию
    public Point3D() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    //Конструктор трехмерной точки
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Расстояние между точками в пространстве то есть 3d точка имеет координаты x, y и z
    //Читать справку https://ru.onlinemschool.com/math/library/analytic_geometry/point_point_length/
    private double distance(Point3D p1, Point3D p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    public double distanceToPoint(Point3D p) {
        return distance(this, p); //this указатель на этот класс.
    }

    //Сдвинуть точку x на shift
    public void shiftX(double shift) {
        this.x += shift;
    }
    //Сдвинуть точку y на shift
    public void shiftY(double shift) {
        this.y += shift;
    }

    //Сдвинуть точку y на shift
    public void shiftZ(double shift) {
        this.z += shift;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "z=" + z +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
