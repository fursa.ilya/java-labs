package com.fursa.task16;

/**
 * Написать программу реализации класса Point3D для трёхмерной точки – наследника класса
 * Point для двумерной точки. Программа должна
 * содержать конструкторы, геттеры, сеттеры, метод toString и два дополнительных метода.
 */
public class Solution {

    //тестируем наши точки и методы описанные в них
    public static void main(String[] args) {
        Point point1 = new Point(1, 10);
        System.out.println(point1); //выводим информацию о точке
        Point point2 = new Point(3, 7);
        System.out.println("Расстояние от точки 1 до точки 2 на плоскости: " + point1.distanceToPoint(point2));
        point1.shiftX(12); //Двигаем первую точку по x
        System.out.println(point1);
        System.out.println("p1: x = " + point1.getX());
        System.out.println("p1: y = " + point1.getY());

        Point3D point3D1 = new Point3D(1, 10, 2);
        System.out.println(point1); //выводим информацию о точке
        Point3D point3D2 = new Point3D(3, 7, 5);
        System.out.println("Расстояние от точки 1 до точки 2 в пространстве: " + point3D1.distanceToPoint(point3D2));
        point3D1.shiftZ(12); //Двигаем первую точку по x
        System.out.println(point3D1);
        System.out.println("p1 3D: x = " + point3D1.getX());
        System.out.println("p1 3D: y = " + point3D1.getY());
        System.out.println("p1 3D: z = " + point3D1.getY());
    }
}
