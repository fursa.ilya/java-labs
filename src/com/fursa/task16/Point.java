package com.fursa.task16;

//класс описывающий обычную точку с координатами x, y
public class Point {
    protected double x; //protected делает переменную доступной в наследниках
    protected double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //Конструктор по умолчанию
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    //Расстояние между двумя точками
    private double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }
    //Расстояние от текущей, до заданной точки
    public double distanceToPoint(Point p) {
        return distance(this, p);
    }

    //Сдвинуть точку x на shift
    public void shiftX(double shift) {
        this.x += shift;
    }
    //Сдвинуть точку y на shift
    public void shiftY(double shift) {
        this.y += shift;
    }
    //геттеры и сеттеры
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    //Переопределили метод toString
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
