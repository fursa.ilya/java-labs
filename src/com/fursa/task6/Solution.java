package com.fursa.task6;

/**
 * Элементы массива циклически сдвинуть на k позиций влево
 */
public class Solution {

    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; //массив чисел для демонстрации сдвига
        int[] newArr = shift(arr, 1); //новый массив

        //вывод массива
        for (int i = 0; i < newArr.length; i++) {
            System.out.print(newArr[i] + " ");
        }
    }

    /**
     * Использовал метод arraycopy, он в реализации имеет модификатор native(т.е он написан не на Java)
     * Советую почитать о его работе подробнее в Google
     * https://www.tutorialspoint.com/java/lang/system_arraycopy.htm
     * src - исходный массив
     * srcPos - начальная позиция в исходном массиве
     * dest - целевой массив, куда производится копирование
     * destPos - позиция в целевом массиве
     * length - длина (сколько должно быть скопировано)
     * сигнатура метода arraycopy выглядит вот так
     * arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
     *
     * step - шаг сдвига
     */
    private static int[] shift(int[] arr, int step) {
        int size = arr.length; //берем размер массива
        int[] newArr = new int[size]; //создаем новый массив, размером как и у предыдущего
        System.arraycopy(arr, step, newArr, 0,size - step);
        System.arraycopy(arr, 0, newArr, size - step, step);
        return newArr;
    }
}
