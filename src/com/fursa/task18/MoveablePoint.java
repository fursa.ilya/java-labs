package com.fursa.task18;

public class MoveablePoint implements Moveable {
    private int x, y, xSpeed, ySpeed;

    public MoveablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void moveUp() {
        y--;
        System.out.println("Перемещен вверх");
    }

    @Override
    public void moveDown() {
        y++;
        System.out.println("Перемещен вниз");
    }

    @Override
    public void moveLeft() {
        x--;
        System.out.println("Перемещен влево");
    }

    @Override
    public void moveRight() {
        x++;
        System.out.println("Перемещен вправо");
    }

    @Override
    public String toString() {
        return "MoveablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                '}';
    }
}
