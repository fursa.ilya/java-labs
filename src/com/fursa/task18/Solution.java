package com.fursa.task18;

/*
Реализовать суперкласс Shape (фигура) и
его подклассы Rectangle (прямоугольник) и
Triangle (треугольник) в соответствии с диаграммой на Рис. 4.2. В тестирующей программе проверить работу
*/
public class Solution {

    public static void main(String[] args) {
        Shape s = new Rectangle("red", 4,5);
        Shape s2 = new Triangle("blue", 4, 5);
        //тестируем
        System.out.println("Rectangle square is " + s.getArea());
        System.out.println("Triangle square is " + s2.getArea());
        System.out.println(s.color);
        System.out.println(s2.color);

        Moveable m1 = new MoveablePoint(5, 6, 10, 4);
        System.out.println(m1);
        m1.moveLeft();
        System.out.println(m1);

        Moveable m2 = new MoveableCircle(2, 1, 2, 20);
        System.out.println(m2);
        m2.moveRight();
        System.out.println(m2);
    }
}
