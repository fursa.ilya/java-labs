package com.fursa.task18;

//интерфейс с методами перемещения фигуры
public interface Moveable {
    public void moveUp();
    public void moveDown();
    public void moveLeft();
    public void moveRight();
}
