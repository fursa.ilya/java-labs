package com.fursa.task18;

public
class Triangle extends Shape {
    private double base;
    private double height;
    private String color;

    public Triangle(String color, double base, double height) {
        super(color);
        this.base = base;
        this.height = height;
    }

    //переопределяем метод вычисления площади. @Override значит что метод переопределенный
    @Override
    double getArea() {
        return (base * height) / 2;
    }

    public String getColor() {
        return color;
    }

}
