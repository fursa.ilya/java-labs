package com.fursa.task18;

import org.w3c.dom.ls.LSOutput;

public class Rectangle extends Shape {
    private double length;
    private double width;

    public Rectangle(String color, double length, double width) {
        super(color);
        this.length = length;
        this.width = width;
    }

    //переопределяем метод вычисления площади
    @Override
    double getArea() {
        return length * width;
    }


}
