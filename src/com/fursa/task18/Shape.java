package com.fursa.task18;

public abstract class Shape {
    protected String color;

    public Shape(String color) {
        this.color = color;
    }

    //Абстрактный метод вычисления площади фигуры,
    //все классы наследники Shape будут переопределять этот метод
    abstract double getArea();


}
