package com.fursa.task3;

import java.util.Scanner;

/**
 * Задана последовательность A={a1,a2, …,an}. Найти номера минимального и максимального
 * из элементов последовательности, попавших в интервал (P,Q).
 */
public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = {1, -3, -5, 9, 2, 20, 18, 90, 0, 20, 50, 13, -1}; //задаем последовательность вручную
        System.out.print("p: ");
        int p = sc.nextInt(); //Вводим с клавиатуры начальную границу последовательности
        System.out.print("q: ");
        int q = sc.nextInt(); //Вводим конечную границу последовательности

        int minValue = 0; //минимальное значение
        int minIndex = 0; //минимальный индекс

        int maxValue = 0; //максимальное значение
        int maxIndex = 0; //максимальный индекс

        if(p >= 0 && q > p && q <= arr.length) { //проверяем существует ли наш интервал
            minValue = arr[p]; //начальное значение
            minIndex = p; //начальный индекс
            for (int i = p; i <= q; i++) { //обычный алгоритм поиска минимума. Проходим массив и считаем, если следующее значение меньше предыдущего, то оно минимум
                if(arr[i] < minValue) {
                    minValue = arr[i]; //присваиваем элемент
                    minIndex = i;     //присваиваем его индекс
                }
            }
            //вывод результата
            System.out.println("Минимум на интервале: " + minValue);
            System.out.println("Индекс минимума на интервале: " + minIndex);
            //аналогично поиску минимума. Единственное изменение в if(arr[i] > maxValue)
            maxValue = arr[p];
            maxIndex = p;
            for (int i = p; i <= q; i++) {
                if(arr[i] > maxValue) {
                    maxValue = arr[i];
                    maxIndex = i;
                }
            }
            //вывод результата
            System.out.println("Максимум на интервале: " + maxValue);
            System.out.println("Индекс максимума на интервале: " + maxIndex);
        }


    }

}
