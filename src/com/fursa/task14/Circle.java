package com.fursa.task14;

public class Circle {
    private double radius;
    private String color;

    //Конструктор без параметров
    public Circle() {
        this.radius = 1.0;
        this.color = "Красный";
    }

    //Конструктор с одним параметром
    public Circle(double radius) {
        this.radius = radius;
        this.color = "Красный";
    }

    //Конструктор с двумя параметрами
    public Circle(double radius, String color) {
        this.color = color;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    //Площадь круга
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
