package com.fursa.task14;

/**
 * Индивидуальное задание. Реализовать один из классов с использованием геттеров,
 * сеттеров, метода toString()  и дополнительно 2-х методов:
 * е) студент;
 */
public class Student {
    private static final String DEFAULT_SPECIALITY = "Programmer";
    private static final String DEFAULT_FIRST_NAME = "Ivan";
    private static final String DEFAULT_LAST_NAME = "Sidorov";

    private String firstName;
    private String lastName;
    private String speciality;

    //Если передается только имя и фамилия в конструктор, т
    //то создается студент со специальностью по умолчанию, это программер
    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = DEFAULT_SPECIALITY;
    }

    //Если аргументы не заданы, то создается студент по умолчанию
    public Student() {
        this.firstName = DEFAULT_FIRST_NAME;
        this.lastName = DEFAULT_LAST_NAME;
        this.speciality = DEFAULT_SPECIALITY;
    }

    public Student(String firstName, String lastName, String speciality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public void printFullname() {
        System.out.println("Студент " + firstName + " " + lastName + " учится на специальности " + speciality);
    }

    public boolean isProgrammer() {
        return this.speciality.equals(DEFAULT_SPECIALITY);
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", speciality='" + speciality + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Student student = new Student();
        student.setFirstName("Alexey");
        student.setLastName("Vershinin");
        student.setSpeciality("Designer");
        System.out.println("Является ли студент программистом? " + student.isProgrammer());
        student.printFullname();
    }
}
