package com.fursa.task14.author;

public class Book {
    private String name;
    private double price;
    private Author author;
    private int qty;

    public Book(String name, double price, Author author, int qty) {
        this.name = name;
        this.price = price;
        this.author = author;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author email =" + author.getEmail() +
                '}';
    }

    public static void main(String[] args) {
        Book dummyBook = new Book("Java для опытных", 210.0, new Author("Опытный автор", "experienced_author@test.com"), 11); //использование анонимного класса
        System.out.println(dummyBook); //Содержимое dummyBook будет выведено в консоль, так как переопределен метод toString()
    }
}
