package com.fursa.task9;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите m: ");
        int m = sc.nextInt();
        System.out.print("Введите n: ");
        int n = sc.nextInt();

        printA(m, n);
        System.out.println();
        printB(m, n);
        System.out.println();
        printC(m, n);
        System.out.println();
        printD(m, n);
    }

    private static void printA(int m, int n) {
        int[][] matrix = new int[m][n];
        int val = 1;
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][matrix.length - i - 1] = val++;
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }

            System.out.println();
        }
    }

    private static void printB(int m, int n) {
        int[][] matrix = new int[m][n];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = j + 1;
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void printC(int m, int n) {
        int[][] matrix = new int[m][n];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix[i].length; j++) {
                matrix[i][j] = j + 1;
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }

            System.out.println();
        }
    }

    private static void printD(int m, int n) {
        int[][] matrix = new int[m][n];
        for (int i = 0; i < matrix.length; i++) {
            int val = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = val < i + 1 ? ++val : val;
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

    }


}
