package com.fursa.task15;

public class Cyclinder extends Circle {
    private double radius;
    private double height;
    private String color;

    public Cyclinder(double radius, double height, String color) {
        this.radius = radius;
        this.height = height;
        this.color = color;
    }

    @Override
    public double getRadius() {
        return super.getRadius();
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * radius * height;
    }

    @Override
    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Cyclinder(radius " + radius
                + " height " + height +
                " color " + color + ");";
    }

    public static void main(String[] args) {
        Cyclinder cyclinder = new Cyclinder(20.2, 10, "Желтый");
        System.out.println(cyclinder);
        System.out.printf( "%.2f", cyclinder.getArea()); //вывод на экран площади

        //todo TestCyclinder? What? Why?
    }
}
