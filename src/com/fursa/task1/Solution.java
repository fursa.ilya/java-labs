package com.fursa.task1;

import java.util.Arrays;

/**
 * Дано 10 чисел. Напечатать сначала все отрицательные, а затем все остальные.
 */

public class Solution {

    public static void main(String[] args) {
        int[] arr = { 0, -3, -5, 10, 11, 5, 90, 32, 14, -4 }; //создаем массив в ручную
        Arrays.sort(arr); //применяем утильный метод sort для сортировки по возрастанию
        for (int i = 0; i < arr.length; i++) { //печатаем массив в консоль
            System.out.print(arr[i] + " ");
        }
    }
}
