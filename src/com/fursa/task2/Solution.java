package com.fursa.task2;

/**
 * Напечатать те элементы массива из 100 элементов,
 * индексы которых являются степенями двойки.
 * https://codereview.stackexchange.com/questions/172849/checking-if-a-number-is-power-of-2-or-not
 */
public class Solution {

    public static void main(String[] args) {
        //Заполняем массив случайными числами в диапазоне от 0 до 100
        int[] arr = new int[100];
        for (int i = 0; i < arr.length; i++) {
            int number = (int) (Math.random() * 100);
            arr[i] = number;
            isPower(arr[i]); //передаем текущий элемент массива в функцию
        }
    }

    private static void isPower(int number) {
        boolean result = number > 0 && ((number & (number - 1)) == 0); //если выражение возвращает true, выводим элемент
        if(result == true) {
            System.out.print(number + " ");
        }
    }
}
