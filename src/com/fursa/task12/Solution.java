package com.fursa.task12;

/**
 * Транспонирование матрицы
 */
public class Solution {

    public static void main(String[] args) {
        int a[][]={{1,2,3},
                   {4,5,6},
                   {7,8,9}}; //Объявляем матрицу
        int transpose[][] = new int[3][3]; //Матрица для результата транспонирования

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                transpose[i][j] = a[j][i]; //просто меняем значения строк на значения столбцов
                System.out.print(transpose[i][j] + " ");
            }
            System.out.println();
        }
    }
}
