package com.fursa.task11;

/**
 * Умножение матрицы на число
 */

public class Solution {

    public static void main(String[] args) {
        int a[][]={{1,2,3}, {4,5,6}, {7,8,9}}; //Объявляем матрицу
        int b[][] = new int[3][3]; //Объявляем матрицу такого же размера для записи результата
        int num = 10; // объявляем число(можно вводить все с клавиатуры, если этого требует задание)
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                b[i][j] = a[i][j] * num; //Аналогично сложению, только теперь ищем произведение элемента матрицы на наше число
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
    }
}
