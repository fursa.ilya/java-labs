package com.fursa.task5;

import java.util.Scanner;

/**
 * Задана последовательность A={a1,a2, …,an}. Найти количество элементов
 * последовательности, попавших в интервал (P,Q).
 */
public class Solution {

    public static void main(String[] args) {
        int arr[] = { 0, 2, 5, 6, 1, 12, 3, 5, 6, 9, 10, 12, 4, 1, -1, -5, 8, 6, 11 };
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите p: ");
        int p = sc.nextInt();
        System.out.print("Введите q: ");
        int q = sc.nextInt();

        System.out.print("Элементы попавшие в интервал: "); //форматирование и вывод
        for (int i = p; i < q; i++) {
            System.out.print(arr[i] + " "); //печатаем элементы массива
        }
        System.out.println("\nКоличество элементов попавших в интервал: " + (q - p)); // от верхней границы, отнимает нижнюю
    }
}
