package com.fursa.task19;

public class ResizableCircle extends Circle implements Resizable {
    private double radius;

    public ResizableCircle(double radius) {
        this.radius = radius;
    }

    @Override
    public void resize(int percent) {
        radius *= percent/100.0;
    }

    @Override
    public double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return "ResizableCircle{" +
                "radius=" + radius +
                '}';
    }
}
