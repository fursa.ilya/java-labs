package com.fursa.task19;

public interface Resizable {
    void resize(int percent);
}
