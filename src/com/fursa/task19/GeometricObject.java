package com.fursa.task19;

public abstract class GeometricObject {
    public abstract double getArea();
    public abstract double getPerimeter();
}
