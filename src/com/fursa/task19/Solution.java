package com.fursa.task19;

/**
 * Написать интерфейсы
 * GeometricObject – «ГеометрическийОбъект» с двумя
 * абстрактными методами getParameter()
 * и getArea(), как указано в диаграмме, а также
 * и Resizable – «ИзменяемыйРазмер» (масштабирование)
 */
public class Solution {

    public static void main(String[] args) {
        GeometricObject geomObj1 = new Circle(5.0);
        System.out.println(geomObj1);
        System.out.println("Perimeter = "+geomObj1.getPerimeter());
        System.out.println("Area = "+geomObj1.getArea());
        //Test resizable circle
        Resizable geomObj2 = new ResizableCircle(5.0);
        System.out.println(geomObj2);
        geomObj2.resize(50);
        System.out.println(geomObj2);
        GeometricObject geomObj3 = (GeometricObject) geomObj2;
        System.out.println(geomObj3);
        System.out.println("Perimeter = "+ geomObj3.getPerimeter());
        System.out.println("Area = "+ geomObj3.getArea());
        //Test circle
        GeometricObject geomObj4 = new Circle(5.0);
        System.out.println(geomObj4);
        System.out.println("Perimeter = "+geomObj4.getPerimeter());
        System.out.println("Area = "+geomObj4.getArea());
    }
}
