package com.fursa.task4;

/**
 * Вывести элемент, ближайший к
 * среднему арифметическому элементов массива.
 */
public class Solution {

    public static void main(String[] args) {
        int[] arr = { 1, 2, 4, 5, 6, 6, 8, 9, 10, 11, 12, 14, 16, 18, 22, 55, 90 };

        int avg = findAvg(arr);
        int result = find(arr, avg);
        System.out.println("Среднее арифметическое: " + avg);
        System.out.println("Ближайший к среднему арифметическому: " + result);
    }

    private static int findAvg(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        return sum / arr.length;
    }

    private static int find(int[] arr, int target) {
        int n = arr.length;

        // Обрабатываем пограничные случаи, если
        // наш элемент находится в начале или конце
        if (target <= arr[0])
            return arr[0];
        if (target >= arr[n - 1])
            return arr[n - 1];

        //применяем бинарный поиск бинарный поиск!
        // Важно: Так как используется бинарный поиск, необходимо отсортировать массив. Без сортировки будет работать неправильно!
        int i = 0, j = n, mid = 0; //i - начало j - конец, mid находим по формуле как в бинарном поиске  (start + end) / 2
        while (i < j) {
            mid = (i + j) / 2;

            if (arr[mid] == target)
                return arr[mid];

            //Если искомый элемент меньше среднего, тогда ищем в левой части
            if (target < arr[mid]) {

                //Если искомый элемент больше чем предыдущий, берем ближайший из двух
                if (mid > 0 && target > arr[mid - 1])
                    return getClosest(arr[mid - 1],
                            arr[mid], target);

                // тоже самое для левой половины
                j = mid;
            }

            // Если искомый больше середины
            else {
                if (mid < n-1 && target < arr[mid + 1])
                    return getClosest(arr[mid],
                            arr[mid + 1], target);
                i = mid + 1; // update i
            }
        }

        // После поиска остался только один элемент, возвращаем его
        return arr[mid];
    }

    //Метод находит ближайший элемент к искомому. Для этого он берет и
    //вычисляет разницу между искомым элементом и обоими значениями
    //Между теми элементами, где максимальна разница находится нужный нам элемент
    private static int getClosest(int i, int j, int target) {
        if(target - i >= j - target) {
            return j;
        }
        else return i;
    }
}
